import json
import sys

from cocluremig.utils.github import find_forks

if __name__ == "__main__":
    params = {'per_page': 100}
    (fluts, raw_flts) = find_forks("defnull", "pixelflut")
    print(fluts)
    with open("flts_forks.json", "w") as writer:
        json.dump(raw_flts, writer, indent=1, ensure_ascii=False)
    sys.exit()
    (mvns, raw_mvns) = find_forks("apache", "maven")
    print(mvns)
    (mvns_full, raw_mvns_full) = find_forks("apache", "maven", params, True)
    print(mvns_full)
    with open("maven_forks.json", "w") as writer:
        json.dump(raw_mvns_full, writer, indent=1, ensure_ascii=False)
    (mv, raw_mv) = find_forks("mediathekview", "MediathekView")
    print(mv)
    with open("mediathekview_forks.json", "w") as writer:
        json.dump(raw_mv, writer, indent=1, ensure_ascii=False)
