from cocluremig.visualization.dot_html_writer import DotHtmlRenderWriter

if __name__ == "__main__":
    writer = open("test.html", "w")
    dw = DotHtmlRenderWriter(writer)
    dw.write_head("test")
    dw.write_simple_render_head("test")
    dw.write_edge(("a", "b"))
    dw.write_simple_render_foot()
    dw.write_footer()
    dw.close()
