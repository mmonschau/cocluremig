"""
Sample clustering application
"""
import csv
import logging
import math
import os
import resource
import sys
from builtins import isinstance
from collections import defaultdict
from typing import Dict, List, Set, Union

import git
import pandas as pd

from cocluremig import graph_statistics
from cocluremig.analyzer.branch_recovery import recover_branches
from cocluremig.analyzer.commit.analyzers import get_basic_commit_data, get_lizard_analyzer_avg
from cocluremig.analyzer.commit.util import AverageElement
from cocluremig.cluster import cluster, create_meta_graph, create_quasi_meta_graph, make_visuable_clustered_graph_step
from cocluremig.utils.gitutils import get_edge_list, get_ref_names, get_repo
from cocluremig.visualization import DOT_COLORS, dot_writer


def topology_based_merge_main(repo: git.Repo, fldr_name: str, url: str):
    """
    Applies a number of pre-definde topology-based-clusterings on the given repo's commit graph
    :param repo: a git repo, whose commit-graph is processed
    :param fldr_name: output-folder name
    :param url: a string used as each output-graph's name
    :return: None
    """
    code_metric_analyzer = get_lizard_analyzer_avg(repo)
    metadata: List[Dict[str, Union[str, bool, int]]] = []
    (edge_list, commits) = get_edge_list(repo)
    for commit in commits:
        sha = commit.hexsha
        data = get_basic_commit_data(commit)
        logging.debug("base data retrieved for commit: %s", sha)
        code_metrics: Dict[str, AverageElement] \
            = code_metric_analyzer.apply_metric(commit)
        data.update(code_metrics)
        logging.debug("metric data retrieved for commit: %s", sha)
        metadata.append(data)
    del code_metric_analyzer
    repo_size = len(edge_list)
    metadata: pd.DataFrame = pd.DataFrame(metadata)
    metadata.to_json(fldr_name + os.path.sep + "data.json", orient="records", lines=True)
    metadata = metadata.set_index("sha")
    logging.info("Finished Repo analyse")

    date_cmpr = lambda x, y, z: 0 - abs(z.loc[x, 'date_authored'] - z.loc[y, 'date_authored'])
    date_cmpr2 = lambda x, y, z: 0 - abs(z.loc[x, 'date_committed'] - z.loc[y, 'date_committed'])
    author_get = lambda x, repo: repo.commit(x).author
    author_cmpr = lambda x, y, repo: 1 if author_get(x, repo) == author_get(y, repo) else 0
    committer_get = lambda x, repo: repo.commit(x).committer
    committer_cmpr = lambda x, y, repo: 1 if committer_get(x, repo) == committer_get(y, repo) else 0

    # minute, hour,day,month,year
    date_steps = [60, 3600, 86400, 604800, 2592000, 31536000]
    cc_steps = [0.00000000001, 0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    cnt_steps = [0, 1, 5, 10, 50, 100, 500, 1000, 5000]
    avg_nloc_steps = [0.00000000001, 0.1, 0.5, 1, 5, 10, 50, 100, 500, 1000, 5000]

    # for calculation with non-exisiting elements
    def ave_cmpr(x: Union[AverageElement, float],
                 y: Union[AverageElement, float],
                 pos: int):
        # if one element is NaN
        if isinstance(x, float) or isinstance(y, float):
            return math.inf
        z1 = 0
        z2 = 0
        if pos == 2:
            z1 = x.avg()
            z2 = y.avg()
        else:
            z1 = x[pos]
            z2 = y[pos]
        return abs(z1 - z2)

    no_cmpr = lambda x, y, z: 0 - ave_cmpr(z[x], z[y], 0)
    el_cnt_cmpr = lambda x, y, z: 0 - ave_cmpr(z[x], z[y], 1)
    ttl_cmpr = lambda x, y, z: 0 - ave_cmpr(z[x], z[y], 2)

    configs = [
        ("authored_date", metadata, date_cmpr, date_steps, 0),
        ("committed_date", metadata, date_cmpr2, date_steps, 0),
        ("author", repo, author_cmpr, 0, 1),
        ("committer", repo, committer_cmpr, 0, 1),
        ("nloc", metadata.loc[:, 'nloc_file'], no_cmpr, cnt_steps, 0),
        ("function_cnt", metadata.loc[:, 'nloc_function'], el_cnt_cmpr, cnt_steps, 0),
        ("token_cnt_func", metadata.loc[:, 'token_count'], no_cmpr, cnt_steps, 0),
        ("cyclomatic_complexity", metadata.loc[:, 'cyclomatic_complexity'], ttl_cmpr, cc_steps, 0),
        ("avg_nloc", metadata.loc[:, 'nloc_function'], ttl_cmpr, avg_nloc_steps, 0)
    ]

    for (name, data_source, heuristic, steps, threshold) in configs:
        logging.info("Clustering by %s", name)
        (clusters, raw) = cluster(edge_list, data_source, heuristic, steps, threshold)
        with open(fldr_name + os.path.sep + "unions_" + name + ".csv", "w") as writer:
            csv_writer = csv.writer(writer,
                                    delimiter=';', quotechar='\"',
                                    quoting=csv.QUOTE_MINIMAL)
            for ((v1, v2), value) in raw.items():
                csv_writer.writerow([v1, v2, value])
        replacement_table = {}
        stats = []
        for (no, c) in enumerate(clusters):
            step_stat = {'no': no}
            if isinstance(steps, list):
                step_stat["step"] = steps[no]
            else:
                step_stat["step"] = steps * no
            cluster_edges, cluster_content, replacement_table = \
                make_visuable_clustered_graph_step(clusters, no, replacement_table)
            cluster_stats = graph_statistics.calculate_cluster_stats(cluster_content.values())
            step_stat.update(cluster_stats._asdict())
            graph_stats = graph_statistics.get_graph_metrics(cluster_edges)
            step_stat.update(graph_stats._asdict())
            step_stat.update(graph_statistics.get_node_types_count(cluster_edges)._asdict())
            stats.append(step_stat)
            with open(fldr_name + os.path.sep + "clusters_" + name + "."
                      + str(no).zfill(repo_size % 10) + ".csv", "w") as writer:
                csv_writer = csv.writer(writer,
                                        delimiter=';', quotechar='\"',
                                        quoting=csv.QUOTE_MINIMAL)
                for (cluster_id, value) in cluster_content.items():
                    csv_writer.writerow([cluster_id] + value)
            with open(fldr_name + os.path.sep +
                      "graph_" + name + "." + str(no).zfill(repo_size % 10)
                      + ".dot", "w") as writer_s:
                d_simple_writer = dot_writer.PrettyDotWriter(writer_s)
                d_simple_writer.write_head(url)
                for (v1, v2) in cluster_edges:
                    d_simple_writer.write_edge((str(v1), str(v2)))
                d_simple_writer.write_footer()
                d_simple_writer.close()
        with open(fldr_name + os.path.sep + "stats." + name + ".csv", "w") as writer:
            csv_writer = csv.DictWriter(writer, fieldnames=stats[0].keys(),
                                        delimiter=';', quotechar='\"',
                                        quoting=csv.QUOTE_MINIMAL)
            csv_writer.writeheader()
            csv_writer.writerows(stats)


def meta_main(repo, fldr_name, url):
    logging.info("Creating Meta Graph")
    (edges, commits) = get_edge_list(repo)
    meta_graph = create_meta_graph(edges)
    meta_edges = [(e1, e2) for (e1, e2, ann) in meta_graph]
    meta_clusters = [ann for (e1, e2, ann) in meta_graph if ann]
    with open(fldr_name + os.path.sep + "unions_meta.csv", "w") as writer:
        csv_writer = csv.writer(writer,
                                delimiter=';', quotechar='\"',
                                quoting=csv.QUOTE_MINIMAL)
        for row in meta_graph:
            csv_writer.writerow([row[0], row[1]] + row[2])
    gm = graph_statistics.get_graph_metrics(meta_edges)
    cc = graph_statistics.calculate_cluster_stats(meta_clusters)
    nt = graph_statistics.get_node_types_count(meta_edges)
    stats = {}
    stats.update(gm._asdict())
    stats.update(cc._asdict())
    stats.update(nt._asdict())
    with open(fldr_name + os.path.sep + "stats.meta.csv", "w") as writer:
        csv_writer = csv.DictWriter(writer, fieldnames=stats.keys(),
                                    delimiter=';', quotechar='\"',
                                    quoting=csv.QUOTE_MINIMAL)
        csv_writer.writeheader()
        csv_writer.writerow(stats)
    with open(fldr_name + os.path.sep + "meta_graph.dot", "w") as writer_s:
        d_writer = dot_writer.PrettyDotWriter(writer_s)
        d_writer.write_head(url)
        for edge in meta_edges:
            d_writer.write_edge(edge)
        d_writer.write_footer()
        d_writer.close()
    quasi_meta, clusters = create_quasi_meta_graph(edges)
    stats = {}
    gm = graph_statistics.get_graph_metrics(quasi_meta)
    cc = graph_statistics.calculate_cluster_stats(clusters.values())
    nt = graph_statistics.get_node_types_count(quasi_meta)
    stats.update(gm._asdict())
    stats.update(cc._asdict())
    stats.update(nt._asdict())
    with open(fldr_name + os.path.sep + "quasi_meta_graph.dot", "w") as writer_s:
        d_writer = dot_writer.PrettyDotWriter(writer_s)
        d_writer.write_head(url)
        for edge in quasi_meta:
            d_writer.write_edge(edge)
        d_writer.write_footer()
        d_writer.close()
    with open(fldr_name + os.path.sep + "stats.quasi_meta.csv", "w") as writer:
        csv_writer = csv.DictWriter(writer, fieldnames=stats.keys(),
                                    delimiter=';', quotechar='\"',
                                    quoting=csv.QUOTE_MINIMAL)
        csv_writer.writeheader()
        csv_writer.writerow(stats)


def graph_branches_main(repo: git.Repo, fldr_name: str, url: str):
    """
    Prints the repos' commit graph with recovered branches marked by color and label
    :param repo: a git repo, whose commit-graph is processed
    :param fldr_name: output-folder name
    :param url: a string used as output-graph's name
    :return: None
    """
    logging.info("recovering Branches")
    (labeled_vertices, edges) = recover_branches(repo)
    branches = sorted(set(labeled_vertices.values()))
    coloring = {b: DOT_COLORS[i % len(DOT_COLORS)] for i, b in enumerate(branches)}
    with open(fldr_name + os.path.sep + "branched_graph.dot", "w") as writer_s:
        d_simple_writer = dot_writer.PrettyDotWriter(writer_s)
        d_simple_writer.write_head(url)
        for e in edges:
            d_simple_writer.write_edge(e)
        for sha, branch in labeled_vertices.items():
            d_simple_writer.write_vertex(sha, sha + "\\n\\n" + branch, coloring[branch])
        d_simple_writer.write_footer()
        d_simple_writer.close()
    branch_replacement_table: Dict[str, Set[str]] = defaultdict(set)
    for vertex, branch in labeled_vertices.items():
        branch_replacement_table[branch].add(vertex)
    stats = {}
    cs = graph_statistics.calculate_cluster_stats(branch_replacement_table.values())
    stats.update(cs._asdict())
    with open(fldr_name + os.path.sep + "stats.branches.csv", "w") as writer:
        csv_writer = csv.DictWriter(writer, fieldnames=stats.keys(),
                                    delimiter=';', quotechar='\"',
                                    quoting=csv.QUOTE_MINIMAL)
        csv_writer.writeheader()
        csv_writer.writerow(stats)
    # merge_branch_edges = cluster_blind(edges, branch_replacement_table)


def basic_view_main(repo: git.Repo, fldr_name: str, url: str):
    """
    Prints the given commit-graph as dot and edge-list & references as csv
    :param repo: a git repo, whose commit-graph is processed
    :param fldr_name: output-folder name
    :param url: a string used as output-graph's name
    :return: None
    """
    logging.info("Getting Graph Base infos")
    (edges, commits) = get_edge_list(repo)
    refs: Dict[str, List[str]] = get_ref_names(repo, False, False)
    with open(fldr_name + os.path.sep + "edges.csv", "w") as writer:
        csv_writer = csv.writer(writer,
                                delimiter=';', quotechar='\"',
                                quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerows(edges)
        writer.write("\n\n")
        for (k, v) in refs.items():
            csv_writer.writerow([k] + v)
    stats = graph_statistics.get_graph_metrics(edges)._asdict()
    stats.update(graph_statistics.get_node_types_count(edges)._asdict())
    with open(fldr_name + os.path.sep + "graph.dot", "w") as writer_s:
        d_simple_writer = dot_writer.PrettyDotWriter(writer_s)
        d_simple_writer.write_head(url)
        for e in edges:
            d_simple_writer.write_edge(e)
        for sha, ref in refs.items():
            d_simple_writer.write_vertex(sha, "//\n".join(ref) + "/\n" + sha, "blue")
        d_simple_writer.write_footer()
        d_simple_writer.close()
    with open(fldr_name + os.path.sep + "stats.graph.csv", "w") as writer:
        csv_writer = csv.DictWriter(writer, fieldnames=stats.keys(),
                                    delimiter=';', quotechar='\"',
                                    quoting=csv.QUOTE_MINIMAL)
        csv_writer.writeheader()
        csv_writer.writerow(stats)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=1)
    resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))
    sys.setrecursionlimit(100000)  # default: 1000
    URLS = []
    with open(sys.argv[1], "r") as reader:
        line = reader.readline()
        while line:
            URLS.append(line.strip())
            line = reader.readline().strip()
    print(URLS)
    if not os.path.exists("../out"):
        os.mkdir("../out")
    for url in URLS:
        logging.info("Processing %s", url)
        fldr_name_prts = url.split("/")[-1].split(".")
        if fldr_name_prts[-1] == "git":
            fldr_name_prts = fldr_name_prts[:-1]
        fldr_name = "out" + os.path.sep + "_".join(fldr_name_prts)
        if not os.path.exists(fldr_name):
            os.mkdir(fldr_name)
        repo = None
        if len(sys.argv) == 2:
            repo = get_repo(url, "data", [], True)
        else:
            repo = get_repo(url, "data", [], True, True)
        logging.debug("Repo cloned/updated")
        basic_view_main(repo, fldr_name, url)
        topology_based_merge_main(repo, fldr_name, url)
        meta_main(repo, fldr_name, url)
        graph_branches_main(repo, fldr_name, url)
